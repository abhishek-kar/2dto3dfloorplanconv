from subprocess import check_output

"""
If we want to use the floorplan in for instance blender we can use this script to format the .blender file as .obj to fetch it in unity!
"""

blender_install_path = "/usr/local/blender/blender"
program_path = ".."

check_output([blender_install_path, "--background", program_path + "/Target/floorplan.blend", "--python", program_path + "/Blender/blender_export_obj_script.py", program_path+"Target/result.obj"])
